An example of the simplest hierarchical model for inferring populations with
measurement uncertainty, focused on posterior predictive checking.

Eventually, this will work in Binder, but right now I can't get it to load.  To
reconstruct the environment, use
[conda](https://docs.conda.io/en/latest/miniconda.html):

    conda env create -f environment.yml

then activate the environment with

    conda activate ghm

Now you are ready to fire up a Jupyter instance and look at the notebook.
