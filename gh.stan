data {
  int nobs;

  real sigma_obs[nobs];
  real xobs[nobs];
}

parameters {
  real mu;
  real<lower=0> sigma;

  real x_unit[nobs];
}

transformed parameters {
  real x[nobs];

  for (i in 1:nobs) {
    x[i] = mu + sigma*x_unit[i];
  }
}

model {
  /* Put unit-scale priors on mu and sigma. */
  mu ~ normal(0, 1);
  sigma ~ normal(0, 2);

  /* Combined with the transformation x = mu + sigma*x_unit this means x ~ N(mu, sigma). */
  x_unit ~ normal(0,1);

  /* Likelihood */
  for (i in 1:nobs) {
    xobs[i] ~ normal(x[i], sigma_obs[i]);
  }
}

generated quantities {
  real x_draw[nobs];

  for (i in 1:nobs) {
    x_draw[i] = normal_rng(mu, sigma);
  }
}
